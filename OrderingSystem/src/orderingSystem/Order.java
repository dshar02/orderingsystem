package orderingSystem;

import java.util.ArrayList;

public class Order 
{
	
	private int orderNumber;
	private ArrayList<MenuItem> customerOrder = new ArrayList<MenuItem>();
	private double total = 0.00;
	
	public Order() 
	{
		
	}

	public int getOrderNumber() 
	{
		return orderNumber;
	}

	public void setOrderNumber(int orderNumber) 
	{
		this.orderNumber = orderNumber;
	}

	public ArrayList<MenuItem> getFoodOrder() 
	{
		return customerOrder;
	}

	public void setFoodOrder(ArrayList<MenuItem> foodOrder) 
	{
		this.customerOrder = foodOrder;
	}
	
	public void addFoodToOrder(MenuItem itemName)
	{
		customerOrder.add(itemName);
		total += itemName.getPrice();
	}
	
	public String toString()
	{
		StringBuilder kitchenOrder = new StringBuilder("***Order Up For Kitchen:***\nOrder Number: " + orderNumber + "\n");
		
		for(MenuItem foodItem: customerOrder)
		{
			kitchenOrder.append("  " + foodItem.getItemName() + "\n");
		}
		return kitchenOrder.toString();
	}
	
	public String getCustomerReceipt()
	{
		StringBuilder customerReceipt = new StringBuilder("***Customer Receipt:***\nOrder Number: " + orderNumber + "\n");
		
		for(MenuItem foodItem: customerOrder)
		{
			customerReceipt.append(String.format("\t%s\t $%,.2f\n", foodItem.getItemName(), foodItem.getPrice()));
		}
		
		customerReceipt.append(String.format("\tTotal:\t\t$%,.2f", total));
		return customerReceipt.toString();
	}
}
