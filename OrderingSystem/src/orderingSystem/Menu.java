package orderingSystem;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Menu 
{
	private ArrayList<MenuItem> menu = new ArrayList<MenuItem>();
	private final String fileName = "data.txt";
	private int itemId = 1;
	
	public Menu()
	{
		MenuFileReader();	
	}
	
	public void MenuFileReader()
	{
		File file;
		
		
		try{
			file = new File (fileName);
			Scanner stdin = new Scanner(file);
			while(stdin.hasNextLine())
			{
				String line =  stdin.nextLine();
				//System.out.println(line);
				
				String[] splitMenuLine = line.split(",");
				
				MenuItem menuItem = new MenuItem(splitMenuLine[0], splitMenuLine[1], itemId, splitMenuLine[2]);
				menu.add(menuItem);
				itemId++;
			}
			stdin.close();
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public void MenuFileWriter()
	{
		try(FileWriter fw = new FileWriter("data.txt", false);
			    BufferedWriter bw = new BufferedWriter(fw);
			    PrintWriter out = new PrintWriter(bw))
			{
			out.print(menu.get(0).toCsv());
				for(int counter = 1; counter<menu.size(); counter++)
				{
					out.print("\n" + menu.get(counter).toCsv());
				}
			    
			} catch (IOException e) {
			    //exception 
			}
	}
	
	public ArrayList<MenuItem> getMenu()
	{
		return menu;
	}
	
	public void addMenuItem(String itemName, double price, int restaurantId)
	{
		MenuItem menuItem = new MenuItem(itemName, price, itemId, restaurantId);
		
		if(menuItem.isValid())
		{
			menu.add(menuItem);
			itemId++;
		}
		else
		{
			System.out.println(itemName + " was not added");
		}
	}
	
	public String deleteMenuItem(int itemId)
	{
		for(int i = 0; i<menu.size(); i++)
		{
			if(menu.get(i).getItemId() == itemId)
			{
				String success = menu.remove(i).getItemName();
				
				return success;
			}
		}
		
		return null;
	}
	
	public String priceChanger(int itemId, double price)
	{
		for(int i = 0; i<menu.size(); i++)
		{
			if(menu.get(i).getItemId() == itemId)
			{
				boolean success = menu.get(i).setPrice(price);
				if(success)
				{
					return menu.get(i).getItemName();
				}
			}
		}
		
		return null;
	}
}
