package orderingSystem;

import javax.swing.JOptionPane;

public class Kitchen 
{
	private Order customerOrder;
	
	
	public Kitchen(Order customerOrder)
	{
		this.customerOrder = customerOrder;
		System.out.println(customerOrder.toString());
	}
	
	public void orderDisposition()
	{
		TV display = new TV(customerOrder.getOrderNumber());
		JOptionPane.showInputDialog("Click OK when order is completed");
		display.setStatus("Completed");
		JOptionPane.showInputDialog("Click OK when order has been picked up");
		display.removeStatus();
	}
}
