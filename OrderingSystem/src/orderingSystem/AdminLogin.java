package orderingSystem;
import javax.swing.JOptionPane;

public class AdminLogin 
{
	static String realUsername = "Admin";
	static String realPassword = "AdminPassword1";

	public static void main(String[] args) 

	{
		String tempUsername;
		String tempPassword;
		boolean exitProgram = false;

		do
		{
			tempUsername = JOptionPane.showInputDialog("Enter Username");
			tempPassword = JOptionPane.showInputDialog("Enter Password");
		}
		while (!tempUsername.equals(realUsername) || !tempPassword.equals(realPassword));

		String existingMenuOptions;
		Menu menu = new Menu();

		do
		{
			existingMenuOptions = JOptionPane.showInputDialog("Would you like to do?\nEnter 1 to Display Menu\n"+ 
					"Enter 2 to Add a NEW menu item\nEnter 3 to Remove/Delete a menu item\n"+
					"Enter 4 to Change price of a menu item\nEnter 5 to Update Changes\nEnter 6 to Exit the Program");
			
			if(existingMenuOptions == null)
			{
				exitProgram = true;
			}
			else if(existingMenuOptions.equals("1"))
				{
				for(MenuItem menuItem: menu.getMenu())
				{
					System.out.println(menuItem.toString());

				}
			}
			else if(existingMenuOptions.equals("2"))
			{
				String newItemName = JOptionPane.showInputDialog("Add name of NEW Menu item.");
				System.out.println(newItemName);

				String newMenuPrice = JOptionPane.showInputDialog("What is the price of NEW Menu item?");
				System.out.println(newMenuPrice);

				String newRestaurantId = JOptionPane.showInputDialog("Please enter the Restaurant ID.\nEnter 1 for 210 Burger Co\nEnter 2 for River City Cafe");
				System.out.println(newRestaurantId);

				menu.addMenuItem(newItemName, Double.parseDouble(newMenuPrice), Integer.parseInt(newRestaurantId));
			}
			else if(existingMenuOptions.equals("3"))
			{
				String deleteItemId = JOptionPane.showInputDialog("What item ID would you like to delete?");
				String success = menu.deleteMenuItem(Integer.parseInt(deleteItemId));
				if(success !=null)
				{
					System.out.println("Item " + success + " has been deleted");
				}
				else
				{
					System.out.println("Item " + deleteItemId + " has NOT been deleted");
				}
			}

			else if(existingMenuOptions.equals("4"))
			{
				String itemIdPriceChanger = JOptionPane.showInputDialog("What is the item ID you want to change the price of?");
				String priceChange = JOptionPane.showInputDialog("What is the new price of the item?");
				String success = menu.priceChanger(Integer.parseInt(itemIdPriceChanger), Double.parseDouble(priceChange));
				
				if(success !=null)
				{
					System.out.println("The price of "+ success + " has been changed to " + priceChange);
				}
				else
				{
					System.out.println("The price of "+ itemIdPriceChanger + " has NOT been changed");
				}
			}
			else if (existingMenuOptions.equals("5"))
			{
				menu.MenuFileWriter();
				System.out.println("Menu update completed");
			}

			else if(existingMenuOptions.equals("6"))
			{
				exitProgram = true;
			}
		}
		while(!exitProgram);

		System.out.println("You have exited the program");
	}
}

