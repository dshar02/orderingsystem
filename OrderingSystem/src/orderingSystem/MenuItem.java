package orderingSystem;

public class MenuItem {
	
	private double price;
	private int itemId;
	private String restaurantName;
	private int restaurantId = 0;
	private final double MAX_PRICE = 10.00;
	private final double MIN_PRICE = 0.79;
	private boolean valid = true;
	private String itemName;
	
	public MenuItem(String itemName, double price, int itemId, int restaurantId) {
		
		setItemName(itemName);
		setPrice(price);
		setItemId(itemId);
		setRestaurantName(restaurantId);
	}
	public MenuItem(String itemName, String strPrice, int itemId, String strRestaurantId)
	{
		this(itemName, Double.parseDouble(strPrice), itemId, Integer.parseInt(strRestaurantId));	
	}
	
	public String getItemName() {
		return itemName;
	}

	public boolean setItemName(String itemName) {
		if(itemName.length() >= 3)
		{
			this.itemName = itemName;
			valid = true && valid;
			return true;
		}
		else
		{
			System.out.println("ERROR: "+ itemName + " was entered. Item name must be at least 3 characters.");
			valid = false;
			return false;
		}
		
	}
	
	public boolean isValid()
	{
		return valid;
	}


	public double getPrice() {
		return price;
	}

	public boolean setPrice(double price) {
		
		if(price > MAX_PRICE || price < MIN_PRICE)
		{
			System.out.printf("ERROR: The amount entered was %.2f. Price must be between %.2f and %.2f\n",price, MIN_PRICE, MAX_PRICE);
			valid = false;
			return false;
		}
		else
		{
			this.price = price;
			valid = true && valid;
			return true;
		}
	}

	public int getItemId() {
		return itemId;
	}

	public void setItemId(int itemId) {
		this.itemId = itemId;
	}
		
	public String getRestaurantName() {
		return restaurantName;
	}

	public boolean setRestaurantName(int restaurantId) {
		if(restaurantId == 1)
		{
			restaurantName = "210 Burger Co.";
			this.restaurantId = restaurantId;
			valid = true && valid;
			return true;
		}
		else if(restaurantId == 2)
		{
			restaurantName = "River City Cafe";
			this.restaurantId = restaurantId;
			valid = true && valid;
			return true;
		}
		/*else if(restaurantId == 3)
		{
			restaurantName = "Houston Street Salads & Subs";
			this.restaurantId = restaurantId;
			valid = true && valid;
			return true;
		}*/
		else
		{
			System.out.printf("ERROR: Restaurant ID entered was %d. Restaurant ID must be 1, or 2.\n", restaurantId);
			valid = false;
			return false;
		}
		
	}
	
	public String toString()
	{
		return String.format("Item ID:%d\t%s,\t\t$%.2f,\t%s",itemId,itemName, price, restaurantName);
	}
	
	public String toCsv()
	{
		return String.format("%s,%.2f,%d",itemName, price, restaurantId);
	}
	
}
