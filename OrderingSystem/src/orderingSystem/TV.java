package orderingSystem;
import java.awt.*;
import javax.swing.*;
public class TV extends JFrame {
	private JLabel orderNumber;
	private JLabel orderNumber2;
	private JLabel status;
	private int order;

	public TV(int order) 
	{
		this.order = order;
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
		this.setSize(1360, 768);
		this.setTitle("Order");

		setLayout(new FlowLayout());

		orderNumber = new JLabel ("Order Number");
		add(orderNumber);
		orderNumber2 = new JLabel (String.valueOf(order));
		add(orderNumber2);

		status = new JLabel ("Pending");
		add(status);
	}
	//TODO Miguel call setStatus and pass in "Completed" when the order is finished.
	public void setStatus(String label)
	{
		status.setText(label);
	}
	
	//TODO Miguel call removeStatus when the customer has picked up the order.
	public void removeStatus()
	{
		status.setText("");
		orderNumber.setText("");
		orderNumber2.setText("");
	}
}
