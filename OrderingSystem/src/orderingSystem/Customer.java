package orderingSystem;

import java.util.ArrayList;
import java.util.Random;

import javax.swing.JOptionPane;

public class Customer 
{
	private static ArrayList<MenuItem> entireMenu;
	private static Menu readMenu = new Menu();

	public static void main(String[] args) 
	{
		String answer;
		double itemPrice = 0;
		Random rand = new Random();
		int orderNumber = rand.nextInt(200);
		String foodName;
		boolean found = false;
		Kitchen kitchen;
		Order orderForKitchen = new Order();
		
		menu();

		do {	
			String foodItem=JOptionPane.showInputDialog("Enter item number to select menu item: ");

			for(MenuItem menuSelection: entireMenu)
			{
				if(menuSelection.getItemId() == Integer.parseInt(foodItem))	
				{
					itemPrice = menuSelection.getPrice();
					foodName = menuSelection.getItemName();

					orderForKitchen.addFoodToOrder(menuSelection);
					System.out.println("Menu item added to customer cart: "+ foodName + ": "+"$"+itemPrice);

					found = true;
					break;
				}
			}
			if(!found)
			{
				System.out.println("ERROR: Item not found!");
			}

			answer= JOptionPane.showInputDialog("Do you want to select another menu item?");
		}while(answer.equalsIgnoreCase("yes")); 			

		if(answer !="yes") 
		{
			orderForKitchen.setOrderNumber(orderNumber);
			System.out.println("\n" + orderForKitchen.getCustomerReceipt() + "\n");
		}
		
		kitchen = new Kitchen(orderForKitchen);
		kitchen.orderDisposition();
	}

	public static void menu()  
	{

		System.out.println("MENU OPTIONS:");
		entireMenu = readMenu.getMenu();

		for(MenuItem menuItem: entireMenu)
		{
			System.out.println(menuItem.toString());
		}
	}
}